#include <stdio.h>
#include <assert.h>
#include "try.h"

int try (struct ctx_s *ctx, func_t *f, int arg) {
  ctx->magic = MAGIC;
  asm("movl %%esp, %0"
      :"=r"(ctx->esp)
      :
      :);
  asm("movl %%ebp, %0"
      :"=r"(ctx->ebp)
      :
      :);
  return f(arg);
}

int throw (struct ctx_s *ctx, int r) {
  assert(ctx->magic == MAGIC);
  asm("movl %0, %%esp"
      :
      :"r"(ctx->esp)
      :);
  asm("movl %0, %%ebp"
      :
      :"r"(ctx->ebp)
      :);
  return r;
}

int mul(int a) {
  return a * 2;
}

int main(void) {
  struct ctx_s ctx;
  int res = try(&ctx, &mul, 4);
  throw(&ctx, res);
  return 0;
}
